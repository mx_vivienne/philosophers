#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <semaphore.h>
# include <stdbool.h>

# define ERR_ARGS	"invalid arguments"
# define ERR_SEM	"could not open new semaphore"

# define SEM_NAME	"/philo_bonus"

# define RNG_MULTIPLIER	6364136223846793005
# define RNG_INCREMENT	1442695040888963407

# define RETRY_MIN_HUNGRY	10
# define RETRY_MAX_HUNGRY	20
# define RETRY_MIN_OK		50
# define RETRY_MAX_OK		100

# define ENV_REDUCED_SPAM	"PHILO_REDUCED_SPAM"

# define MSG_SLEEP	"is sleeping"
# define MSG_THINK	"is thinking"
# define MSG_EAT	"is eating"
# define MSG_DIE	"died"
# define MSG_FORK	"has taken a fork"
# define MSG_FORK2	"has taken both forks"

typedef struct s_rules
{
	long	philosophers;
	long	time_to_die;
	long	time_to_eat;
	long	time_to_sleep;
	long	minimum_meals;
}	t_rules;

typedef	struct s_rng
{
	unsigned int	state;
}	t_rng;

typedef struct s_pers
{
	long			id;
	long			last_meal;
	long			start;
	bool			reduced_spam;
	t_rules			*rules;
	sem_t			*forks;
}	t_pers;

bool			parse_args(int argc, char *argv[], t_rules *r);
long			get_timestamp();
void			rng_init(t_rng *rng, unsigned int seed);
unsigned int	rng_rand(t_rng *rng);
unsigned int	rng_range(t_rng *rng, unsigned int min, unsigned int max);
bool			create_processes(t_pers *info, pid_t **processes);
void			wait_for_processes(pid_t *processes);
void			philosopher_start(t_pers *info);
void			start_eating(t_pers *info);
void			announce(t_pers *info, char *message);
void			announce_debug(t_pers *info, char *message, long extra);
size_t			ft_strlen(char *s);
int				ft_strncmp(char *s1, char *s2, size_t n);

#endif
