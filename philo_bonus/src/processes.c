#include "philosophers.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>

void	wait_for_processes(pid_t *processes)
{
	pid_t	exited;
	int		wstatus;

	exited = waitpid(-1, &wstatus, 0);
	while (*processes)
	{
		if (*processes != exited)
			kill(*processes, SIGKILL);
		processes++;
	}
}

static bool	fork_process(t_pers *info, pid_t **processes, long index)
{
	(*processes)[index] = fork();
	if ((*processes)[index] == 0)
	{
		free(*processes);
		philosopher_start(info);
		exit(1);
	}
	return ((*processes)[index] > 0);
}

static void	stop_processes(pid_t *processes)
{
	while (*processes > 0)
	{
		kill(*processes, SIGKILL);
		processes++;
	}
}

bool	create_processes(t_pers *info, pid_t **processes)
{
	long	num;

	*processes = malloc(sizeof(pid_t) * info->rules->philosophers);
	if (!(*processes))
		return (false);
	memset(*processes, 0, sizeof(pid_t) * info->rules->philosophers);
	num = 0;
	while (num < info->rules->philosophers)
	{
		info->id = num + 1;
		info->last_meal = get_timestamp();
		if (!fork_process(info, processes, num))
		{
			stop_processes(*processes);
			free(processes);
			return (false);
		}
		num++;
	}
	return (true);
}