#include "philosophers.h"
#include <semaphore.h>
#include <unistd.h>
#include <stdio.h>

static void	eat_spaghetti(t_pers *info, long eat_start)
{
	long	now;

	announce(info, MSG_EAT);
	info->last_meal = eat_start;
	while (1)
	{
		now = get_timestamp();
		if (now >= eat_start + info->rules->time_to_eat)
			return ;
		usleep((eat_start + info->rules->time_to_eat - now) * 1000);
	}
}

void	start_eating(t_pers *info)
{
	sem_wait(info->forks);
	if (!info->reduced_spam)
		announce(info, MSG_FORK);
	sem_wait(info->forks);
	if (!info->reduced_spam)
		announce(info, MSG_FORK);
	else
		announce(info, MSG_FORK2);
	eat_spaghetti(info, get_timestamp());
	sem_post(info->forks);
	sem_post(info->forks);
}
