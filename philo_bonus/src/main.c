#include "philosophers.h"
#include <semaphore.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

static int	put_error(char *cmd, char *msg)
{
	write(2, cmd, ft_strlen(cmd));
	write(2, ": ", 2);
	write(2, msg, ft_strlen(msg));
	write(2, "\n", 1);
	return (EXIT_FAILURE);
}

long	get_timestamp()
{
	struct timeval	now;

	gettimeofday(&now, NULL);
	return (now.tv_sec * 1000 + now.tv_usec / 1000);
}

static bool	use_reduced_spam(char *envp[])
{
	size_t	len;

	while (*envp)
	{
		len = ft_strlen(ENV_REDUCED_SPAM);
		if (ft_strncmp(ENV_REDUCED_SPAM, *envp, len) == 0 && (*envp)[len + 1])
			return (true);
		envp++;
	}
	return (false);
}

int	main(int argc, char *argv[], char *envp[])
{
	t_pers			info;
	pid_t			*processes;
	long			now;
	t_rules			rules;
	size_t			value;

	if (!parse_args(argc, argv, &rules))
		return (put_error(argv[0], ERR_ARGS));
	now = get_timestamp();
	sem_unlink(SEM_NAME);
	info = (t_pers){0, now, now, use_reduced_spam(envp), &rules,
		sem_open(SEM_NAME, O_CREAT | O_EXCL, 0644, 0)};
	if (info.forks == SEM_FAILED)
		return (put_error(argv[0], ERR_SEM));
	value = 0;
	while (value < (size_t)rules.philosophers)
	{
		sem_post(info.forks);
		value++;
	}
	processes = NULL;
	if (!create_processes(&info, &processes))
	{
		free(processes);
		sem_close(info.forks);
		sem_unlink(SEM_NAME);
		return (EXIT_FAILURE);
	}
	wait_for_processes(processes);
	free(processes);
	sem_close(info.forks);
	sem_unlink(SEM_NAME);
	return (EXIT_SUCCESS);
}
