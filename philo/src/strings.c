#include <stddef.h>

size_t	ft_strlen(char *s)
{
	size_t	length;

	length = 0;
	while (s[length])
		length++;
	return (length);
}

int	ft_strncmp(char *s1, char *s2, size_t n)
{
	size_t  offset;

	offset = 0;
	while (offset < n)
	{
		if (s1[offset] != s2[offset])
			return ((unsigned char)s1[offset] - (unsigned char)s2[offset]);
		if (s1[offset] == '\0')
			break ;
		offset++;
	}
	return (0);
}
