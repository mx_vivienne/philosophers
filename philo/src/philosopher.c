#include "philosophers.h"
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>

static void	goto_sleep(t_pers *info, long sleep_start)
{
	long	now;

	announce(info, MSG_SLEEP);
	while (1)
	{
		now = get_timestamp();
		if (now >= sleep_start + info->rules->time_to_sleep)
			return ;
		usleep((sleep_start + info->rules->time_to_sleep - now) * 1000);
	}
}

void	announce(t_pers *info, char *message)
{
	struct timeval	now;
	long			milli;
	long			micro;

	gettimeofday(&now, NULL);
	milli = now.tv_sec * 1000 + now.tv_usec / 1000;
	micro = now.tv_usec % 1000;
	printf("%7ld.%03ld %ld %s\n",
		milli - info->shared->start,
		micro, info->id, message);
}

void	announce_debug(t_pers *info, char *message, long extra)
{
	(void)info;
	(void)message;
	(void)extra;
	/*
	struct timeval	now;
	long			milli;
	long			micro;

	gettimeofday(&now, NULL);
	milli = now.tv_sec * 1000 + now.tv_usec / 1000;
	micro = now.tv_usec % 1000;
	printf("%7ld.%03ld %ld %s (%ld)\n",
		milli - info->shared->start,
		micro, info->id, message, extra);
	*/
}

void	philosopher_start(t_pers *info)
{
	long	now;
	long	wait_time;

	while (1)
	{
		if (info->shared->state == STATE_END)
			break ;
		now = get_timestamp();
		if (now - info->last_meal >= info->rules->time_to_die)
		{
			announce(info, MSG_DIE);
			info->shared->state = STATE_END;
			break ;
		}
		if (try_eating(info, &wait_time))
		{
			goto_sleep(info, get_timestamp());
			announce(info, MSG_THINK);
		}
		else
		{
			announce_debug(info, "couldnt eat, waiting", wait_time);
			usleep(wait_time);
		}
	}
}