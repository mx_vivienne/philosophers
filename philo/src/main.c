#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "philosophers.h"

static int	put_error(char *cmd, char *msg)
{
	write(2, cmd, ft_strlen(cmd));
	write(2, ": ", 2);
	write(2, msg, ft_strlen(msg));
	write(2, "\n", 1);
	return (EXIT_FAILURE);
}

long	get_timestamp()
{
	struct timeval	now;

	gettimeofday(&now, NULL);
	return (now.tv_sec * 1000 + now.tv_usec / 1000);
}

int	main(int argc, char *argv[], char *envp[])
{
	t_rules			rules;
	t_shared		shared;
	pthread_t		*threads;

	if (!parse_args(argc, argv, &rules))
		return (put_error(argv[0], ERR_ARGS));
	threads = malloc(sizeof(pthread_t) * rules.philosophers);
	if (!threads)
		return (EXIT_FAILURE);
	if (!init_shared(&rules, &shared, envp))
	{
		free(threads);
		return (EXIT_FAILURE);
	}
	if (!create_threads(&rules, &shared, threads)
		|| !join_threads(&rules, threads))
	{
		free(threads);
		destroy_shared(&shared);
		return (EXIT_FAILURE);
	}
	free(threads);
	destroy_shared(&shared);
	return (EXIT_SUCCESS);
}

void	*thread_start(void *arg)
{
	t_pers			*info;

	info = arg;
	info->last_meal = get_timestamp();
	if (info->id % 2 == 0)
		usleep(200);
	philosopher_start(info);
	return (NULL);
}