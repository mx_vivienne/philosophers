#include "philosophers.h"

void	rng_init(t_rng	*rng, unsigned int seed)
{
	rng->state = seed;
}

unsigned int	rng_rand(t_rng *rng)
{
	rng->state = (RNG_MULTIPLIER * rng->state + RNG_INCREMENT);
	return (rng->state);
}

unsigned int	rng_range(t_rng *rng, unsigned int min, unsigned int max)
{
	return (rng_rand(rng) % (max - min) + min);
}