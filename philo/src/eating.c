#include "philosophers.h"
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

static void	eat_spaghetti(t_pers *info, long eat_start)
{
	long	now;

	//printf("%7ld %ld is eating\n",
	//	eat_start - info->shared->start, info->id);
	announce(info, MSG_EAT);
	info->last_meal = eat_start;
	while (1)
	{
		now = get_timestamp();
		if (now >= eat_start + info->rules->time_to_eat)
			return ;
		usleep((eat_start + info->rules->time_to_eat - now) * 1000);
	}
}

static bool	try_fork(t_pers *info, long id)
{
	bool	fork_available;

	announce_debug(info, "wants a fork", id);
	pthread_mutex_lock(info->shared->mutices + id);
	fork_available = info->shared->forks[id] == 0;
	if (fork_available)
	{
		info->shared->forks[id] = info->id;
		if (!info->shared->reduced_spam)
			announce(info, MSG_FORK);
	}
	pthread_mutex_unlock(info->shared->mutices + id);
	return (fork_available);
}

bool	try_eating(t_pers *info, long *wait_time)
{
	double	hunger;

	hunger = (double)(get_timestamp() - info->last_meal)
		/ (double)info->rules->time_to_die;
	*wait_time = rng_range(&(info->shared->rng),
		(long)(RETRY_MIN_HUNGRY * hunger + RETRY_MIN_OK * (1.0 - hunger)),
		(long)(RETRY_MAX_HUNGRY * hunger + RETRY_MAX_OK * (1.0 - hunger)));
	if (!try_fork(info, info->id - 1))
		return (false);
	if (!try_fork(info, info->id % info->shared->amount))
	{
		announce_debug(info, "puts back fork", info->id - 1);
		info->shared->forks[info->id - 1] = 0;
		return (false);
	}
	if (info->shared->reduced_spam)
		announce(info, MSG_FORK2);
	eat_spaghetti(info, get_timestamp());
	announce_debug(info, "puts back fork", info->id - 1);
	info->shared->forks[info->id - 1] = 0;
	announce_debug(info, "puts back fork", info->id % info->shared->amount);
	info->shared->forks[info->id % info->shared->amount] = 0;
	return (true);
}