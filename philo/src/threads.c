#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include "philosophers.h"

bool	create_threads(t_rules *rules, t_shared *shared, pthread_t *threads)
{
	t_pers	*info;
	long	number;

	number = 0;
	shared->start = get_timestamp();
	while (number < rules->philosophers)
	{
		info = malloc(sizeof(t_pers));
		if (!info)
			return (false);
		*info = (t_pers){number + 1, 0, rules, shared};
		if (pthread_create(threads + number, NULL, &thread_start, info) != 0)
			return (false);
		number++;
	}
	return (true);
}

bool	join_threads(t_rules *rules, pthread_t *threads)
{
	long	number;

	number = 0;
	while (number < rules->philosophers)
	{
		if (pthread_join(threads[number], NULL) != 0)
			return (false);
		number++;
	}
	return (true);
}
