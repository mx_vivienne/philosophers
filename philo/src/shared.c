#include "philosophers.h"
#include <sys/time.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>

static bool	shared_alloc(t_rules *rules, t_shared *shared)
{
	shared->forks = malloc(sizeof(long) * rules->philosophers);
	shared->mutices = malloc(sizeof(pthread_mutex_t) * rules->philosophers);
	if (!shared->forks || !shared->mutices)
	{
		free(shared->forks);
		free(shared->mutices);
		return (false);
	}
	return (true);
}

static bool	use_reduced_spam(char *envp[])
{
	size_t	len;

	while (*envp)
	{
		len = ft_strlen(ENV_REDUCED_SPAM);
		if (ft_strncmp(ENV_REDUCED_SPAM, *envp, len) == 0 && (*envp)[len + 1])
			return (true);
		envp++;
	}
	return (false);
}

bool	init_shared(t_rules *rules, t_shared *shared, char *envp[])
{
	size_t			offset;
	struct timeval	tv;

	*shared = (t_shared){0, NULL, NULL, {0}, 0, STATE_REGULAR,
		use_reduced_spam(envp)};
	gettimeofday(&tv, NULL);
	rng_init(&(shared->rng), (unsigned int)(1000000 * tv.tv_sec + tv.tv_usec));
	if (!shared_alloc(rules, shared))
		return (false);
	offset = 0;
	while (offset < (size_t)rules->philosophers)
	{
		shared->forks[offset] = 0;
		if (pthread_mutex_init(shared->mutices + offset, NULL) != 0)
		{
			destroy_shared(shared);
			return (false);
		}
		shared->amount++;
		offset++;
	}
	return (true);
}

void	destroy_shared(t_shared *shared)
{
	size_t	offset;

	offset = 0;
	while (offset < shared->amount)
	{
		pthread_mutex_destroy(shared->mutices + offset);
		offset++;
	}
	free(shared->mutices);
	free(shared->forks);
}