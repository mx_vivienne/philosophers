#include <stdbool.h>
#include "philosophers.h"

static bool	ft_isspace(char c)
{
	return (c == ' ' || c == '\t' || c == '\n'
			|| c == '\f' || c == '\r' || c == '\v');
}

static bool	parse_int(char *num, long *result)
{
	int	sign;

	sign = 1;
	*result = 0;
	while (ft_isspace(*num))
		num++;
	while (*num == '+' || *num == '-')
	{
		sign *= *num == '-' * -1;
		num++;
	}
	while (*num >= '0' && *num <= '9')
	{
		*result = (10 * *result) + (*num - '0');
		num++;
	}
	*result *= sign;
	while (ft_isspace(*num))
		num++;
	return (*num == '\0');
}

bool	parse_args(int argc, char *argv[], t_rules *r)
{
	r->minimum_meals = -1;
	return (argc >= 5
			&& parse_int(argv[1], &(r->philosophers)) && r->philosophers > 1
			&& parse_int(argv[2], &(r->time_to_die)) && r->time_to_die > 0
			&& parse_int(argv[3], &(r->time_to_eat)) && r->time_to_eat > 0
			&& parse_int(argv[4], &(r->time_to_sleep)) && r->time_to_sleep > 0
			&& (argc == 5 || (parse_int(argv[5], &(r->minimum_meals)) 
				&& r->minimum_meals >= 0)));
}