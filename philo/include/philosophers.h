#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <stdbool.h>
# include <pthread.h>
#include <sys/select.h>

# define ERR_ARGS	"invalid arguments"

# define RNG_MULTIPLIER	6364136223846793005
# define RNG_INCREMENT	1442695040888963407

# define RETRY_MIN_HUNGRY	10
# define RETRY_MAX_HUNGRY	20
# define RETRY_MIN_OK		50
# define RETRY_MAX_OK		100

# define ENV_REDUCED_SPAM	"PHILO_REDUCED_SPAM"

# define MSG_SLEEP	"is sleeping"
# define MSG_THINK	"is thinking"
# define MSG_EAT	"is eating"
# define MSG_DIE	"died"
# define MSG_FORK	"has taken a fork"
# define MSG_FORK2	"has taken both forks"

typedef enum e_state
{
	STATE_REGULAR,
	STATE_END
}	t_state;

typedef struct s_rules
{
	long	philosophers;
	long	time_to_die;
	long	time_to_eat;
	long	time_to_sleep;
	long	minimum_meals;
}	t_rules;

typedef	struct s_rng
{
	unsigned int	state;
}	t_rng;

typedef struct s_shared
{
	size_t			amount;
	pthread_mutex_t	*mutices;
	long			*forks;
	t_rng			rng;
	long			start;
	t_state			state;
	bool			reduced_spam;
}	t_shared;

typedef struct s_pers
{
	long			id;
	long			last_meal;
	t_rules			*rules;
	t_shared		*shared;
}	t_pers;

bool			parse_args(int argc, char *argv[], t_rules *r);
long			get_timestamp();
void			rng_init(t_rng	*rng, unsigned int seed);
unsigned int	rng_rand(t_rng *rng);
unsigned int	rng_range(t_rng *rng, unsigned int min, unsigned int max);
bool			init_shared(t_rules *rules, t_shared *shared, char *envp[]);
void			destroy_shared(t_shared *shared);
bool			create_threads(t_rules *rules, t_shared *shared,
					pthread_t *threads);
bool			join_threads(t_rules *rules, pthread_t *threads);
void			*thread_start(void *arg);
void			philosopher_start(t_pers *info);
bool			try_eating(t_pers *info, long *wait_time);
void			announce(t_pers *info, char *message);
void			announce_debug(t_pers *info, char *message, long extra);
size_t			ft_strlen(char *s);
int				ft_strncmp(char *s1, char *s2, size_t n);

#endif
